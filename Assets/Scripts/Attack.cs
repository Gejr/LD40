﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
	PlayerController playerController;
	public Chase homeless;
	float oldHomelessSpeed;

	void Start()
	{
		playerController = FindObjectOfType<PlayerController> ();
		homeless = FindObjectOfType<Chase> (); 
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "PlayerCollider")
		{
			playerController.TakeDamage ();
			homeless.StartFreeze ();
		}
	}
}
