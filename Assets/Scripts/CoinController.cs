﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
	public GameObject coin;
	AudioSource audio;
	public GameObject[] coins;
	public Vector3 spawnLoc;

	void Start()
	{
		audio = GetComponent<AudioSource> ();
	}

	public void SpawnNewCoin()
	{
		spawnLoc = new Vector3 (Random.Range (-3.4f, 3.4f), (int)Random.Range (-1.8f, 1.3f));
		Instantiate (coin, spawnLoc, transform.rotation);
	}

	public void playEffect()
	{
		audio.Play();

	}

	void Update()
	{
		coins = GameObject.FindGameObjectsWithTag ("Coin");
		if(coins.Length > 1)
		{
			Destroy (coins [0]);
		}
	}
}
