﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class backToMenu : MonoBehaviour {

	public GameObject panel;
	public GameObject panel2;
	public GameObject panel3;
	public GameObject panel4;
	public TMP_Text high;
	AudioSource audio;

	void Start()
	{
		audio = GetComponent<AudioSource> ();
	}

	public void tryAgain()
	{
		SceneManager.LoadScene ("menu");
		audio.Play ();
	}
	public void play()
	{
		SceneManager.LoadScene ("main");
		audio.Play ();
	}
	public void quit()
	{
		audio.Play ();
		Application.Quit ();
	}
	public void help()
	{
		panel.SetActive (false);
		panel2.SetActive (true);
		audio.Play ();
	}
	public void helpBack()
	{
		panel.SetActive (true);
		panel2.SetActive (false);
		audio.Play ();
	}

	public void highScore()
	{
		panel3.SetActive (false);
		panel4.SetActive (true);
		high.text = PlayerPrefs.GetInt("HighScore",0).ToString();
		audio.Play ();
	}

	public void highScoreBack()
	{
		panel3.SetActive (true);
		panel4.SetActive (false);
		audio.Play ();
	}
}
