﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trigger : MonoBehaviour {

	PlayerController playerController;
	CoinController coinController;
	SpriteRenderer homeless;
	public GameObject coin;
	Animator anim;
	trigger trig;
	AudioSource audio;
	BoxCollider2D col;
	GameObject player;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		playerController = FindObjectOfType<PlayerController> ();
		coinController = FindObjectOfType<CoinController> ();
		anim = coin.GetComponent<Animator> ();
		trig = coin.GetComponent<trigger> ();
		audio = coin.GetComponent<AudioSource> ();
		col = coin.GetComponent<BoxCollider2D> ();
	}

	void Spawn()
	{
		if(player.transform.position.x >= 0)
		{
			Vector3 position = new Vector3(Random.Range (-3.4f, -0.1f), Random.Range (-1.8f, 1.3f));
			anim.enabled = true;
			trig.enabled = true;
			audio.enabled = true;
			col.enabled = true;
			Instantiate (coin, position, Quaternion.identity);
		}
		if(player.transform.position.x < 0)
		{
			Vector3 position = new Vector3(Random.Range (0.1f, 3.4f), Random.Range (-1.8f, 1.3f));
			anim.enabled = true;
			trig.enabled = true;
			audio.enabled = true;
			col.enabled = true;
			Instantiate (coin, position, Quaternion.identity);
		}

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "PlayerCollider")
		{
			playerController.AddMoney ();
			Destroy (this.gameObject);
			Spawn ();
		}
	}
}
