﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	public int money;
	int i = 0;
	int h = 2;
	public int health = 100;
	public TMP_Text coinsText;
	public TMP_Text healthText;

	public CoinController coinController;
	public HomelessController homelessController;
	GameObject player;
	Rigidbody2D rigid;
	public GameObject coll;
	AudioSource audio;
	public GameObject death;
	AudioSource pAudio;

	void Start()
	{
		homelessController = FindObjectOfType<HomelessController> ();
		coinController = FindObjectOfType<CoinController> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		audio = GetComponent<AudioSource> ();
		pAudio = death.GetComponent<AudioSource> ();
		DontDestroyOnLoad (death);
		coll = GameObject.FindGameObjectWithTag ("PlayerCollider");
	}

	public void AddMoney()
	{
		money += 1;
		coinController.playEffect ();
		coinsText.text = money.ToString();
		homelessController.SpawnHomeless ();
		AddHealth ();
	}
	public void TakeDamage()
	{
		audio.Play();
		health -= 4;
		healthText.text = health.ToString ();
		coll.SetActive (false);
		StartCoroutine (CoolDown ());
	}

	void AddHealth()
	{
		health += h;
		healthText.text = health.ToString ();
	}

	void Update()
	{
		if(health <= 0)
		{
			Die ();
		}
	}
	IEnumerator CoolDown()
	{
		yield return new WaitForSeconds(2);
		coll.SetActive (true);
	}

	void Die()
	{
		if(money > PlayerPrefs.GetInt("HighScore", 0))
		{
			PlayerPrefs.SetInt ("HighScore", money);
		}
		pAudio.Play ();
		SceneManager.LoadScene("dead");
	}
}
