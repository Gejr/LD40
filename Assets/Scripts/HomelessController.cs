﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomelessController : MonoBehaviour {

	public GameObject homeless;
	public GameObject[] homelessPeople;
	public PlayerController playerController;
	GameObject player;
	public Vector3 spawnLoc;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		playerController = FindObjectOfType<PlayerController> ();
	}

	void Update()
	{

//		if(player.transform.position.x >= 0)
//		{
//			spawnLoc = new Vector3 (Random.Range (-3.4f, -0.1f), Random.Range (-1.8f, 1.3f));
//			Instantiate (homeless, spawnLoc, Quaternion.identity);
//		}
//		if(player.transform.position.x < 0)
//		{
//			spawnLoc = new Vector3 (Random.Range (0.1f, 3.4f), Random.Range (-1.8f, 1.3f));
//			Instantiate (homeless, spawnLoc, Quaternion.identity);
//		}

		homelessPeople = GameObject.FindGameObjectsWithTag ("Homeless");
		if(homelessPeople.Length > playerController.money)
		{
			Destroy (homelessPeople [0]);
		}
	}

	public void SpawnHomeless()
	{
		if(player.transform.position.x >= 0)
		{
			spawnLoc = new Vector3 (Random.Range (-3.4f, -0.1f), Random.Range (-1.8f, 1.3f));
			Instantiate (homeless, spawnLoc, Quaternion.identity);
		}
		if(player.transform.position.x < 0)
		{
			spawnLoc = new Vector3 (Random.Range (0.1f, 3.4f), Random.Range (-1.8f, 1.3f));
			Instantiate (homeless, spawnLoc, Quaternion.identity);
		}
	}
}
