﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour {

	public float speed;
	public float radius = 5f;
	public float power = 100f;

	public Animator anim;
	public Rigidbody2D rigid;
	public Collider2D[] colliders;

	public PlayerController playerController;
	public bool isLeaping;

	public GameObject panel;
	public Slider slider;

	void Start()
	{
		isLeaping = false;
		playerController = FindObjectOfType<PlayerController> ();
		anim = GetComponent<Animator> ();
		rigid = GetComponent<Rigidbody2D> ();
	}

	void Update ()
	{
		if(Input.GetKey(KeyCode.D) )
		{
			anim.SetBool("running", true);
			anim.SetInteger("faceDir", 1);
			transform.Translate (new Vector3 (speed, 0, 0));
//			rigid.MovePosition(new Vector2(speed,0));
			if(Input.GetKey(KeyCode.W))
			{
				transform.Translate (new Vector3 (0, speed, 0));
			}
			if(Input.GetKey(KeyCode.S))
			{
				transform.Translate (new Vector3 (0, -speed, 0));
			}

		}
		else if(Input.GetKey(KeyCode.A))
		{
			anim.SetBool("running", true);
			anim.SetInteger("faceDir", 0);
			transform.Translate (new Vector3 (-speed, 0, 0));
			if(Input.GetKey(KeyCode.W))
			{
				transform.Translate (new Vector3 (0, speed, 0));
			}
			if(Input.GetKey(KeyCode.S))
			{
				transform.Translate (new Vector3 (0, -speed, 0));
			}
		}
		else if(Input.GetKey(KeyCode.W))
		{
			anim.SetBool("running", true);
			transform.Translate (new Vector3 (0, speed, 0));
		}
		else if(Input.GetKey(KeyCode.S))
		{
			anim.SetBool("running", true);
			transform.Translate (new Vector3 (0, -speed, 0));
		}
		else
		{
			anim.SetBool("running", false);
		}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			if(isLeaping == false)
			{
				StartCoroutine (Leap ());
				print("Running Coroutine");
			}
		}

		float timer = 0.2f * Time.deltaTime;
		slider.value += timer;
	}

	void Push()
	{
		Vector2 explosionPos = transform.position;
		colliders = Physics2D.OverlapCircleAll(explosionPos, radius);

		foreach (Collider2D hit in colliders)
		{
			Rigidbody2D rb = hit.GetComponent<Rigidbody2D> ();

			if(rb != null)
			{
				if(rb.tag == "Homeless")
				{
					rb.AddExplosionForce (power, explosionPos, radius, 3f);
//					playerController.health -= 0.1f;
				}
			}
		}
	}

	IEnumerator Leap()
	{
		isLeaping = true;
		speed = 0.06f;
		print("Increased Speed, 1 second remaining");
		yield return new WaitForSeconds (1);
		speed = 0.025f;
		print ("Decreased Speed, 5 seconds cooldown");
		StartCoroutine(DecreaseTimer ());
		yield return new WaitForSeconds (5);
		panel.SetActive (false);
		isLeaping = false;
		print("Ready: Cooldown finished");
	}

	IEnumerator DecreaseTimer()
	{
		print ("DecreaseTimer");
		panel.SetActive (true);
		if(slider != null)
		{
//			float timer = 0.1f;
			slider.value = 0f;
//			while (slider.value != 1)
//			{
//				slider.value += timer;
////				yield return new WaitForSeconds (0.1f);
//				if(slider.value <= 1)
//				{
//					break;
//				}
//			}
		}
		yield return null;
	}
}

public static class Rigidbody2DExtension
{
    public static void AddExplosionForce(this Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius)
    {
        var dir = (body.transform.position - explosionPosition);
        float wearoff = 1 - (dir.magnitude / explosionRadius);
		body.AddForce(dir.normalized * explosionForce * wearoff);
    }
    
    public static void AddExplosionForce(this Rigidbody2D body, float explosionForce, Vector3 explosionPosition, float explosionRadius, float upliftModifier)
    {
        var dir = (body.transform.position - explosionPosition);
        float wearoff = 1 - (dir.magnitude / explosionRadius);
        Vector3 baseForce = dir.normalized * explosionForce * wearoff;
        body.AddForce(baseForce);
 
        float upliftWearoff = 1 - upliftModifier / explosionRadius;
        Vector3 upliftForce = Vector2.up * explosionForce * upliftWearoff;
        body.AddForce(upliftForce);
    }
}

