﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : MonoBehaviour {

	public float speed;
	public Animator anim;
	public GameObject player;

	void Start()
	{
		anim = GetComponent<Animator> ();
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void Update()
	{
		if (speed > 0) {
			Vector3 localPosition = player.transform.position - transform.position;
			localPosition = localPosition.normalized;
			transform.Translate (localPosition.x * Time.deltaTime * speed,
				localPosition.y * Time.deltaTime * speed,
				localPosition.z * Time.deltaTime * speed);

			if (localPosition.x < 0) {
				anim.SetBool ("running", true);
				anim.SetInteger ("faceDir", 0);
			} else if (localPosition.x > 0) {
				anim.SetBool ("running", true);
				anim.SetInteger ("faceDir", 1);
			}
		}
	}

	public void StartFreeze()
	{
		StartCoroutine (Freeze ());
	}

	IEnumerator Freeze()
	{
		speed = 0f;
		yield return new WaitForSeconds (2);
		speed = 1;
	}
}
